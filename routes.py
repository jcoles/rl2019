import networkx as nx
import matplotlib.pyplot as plt
import random
import numpy as np
class OnlineRoute():
    def __init__(self, walks, nodes, minAvg, avgRange, minStddev, stdRange):
        self.G = nx.DiGraph()
        self.start = 0
        self.goal = nodes-1
        self.nodes = nodes
        self.distributions = {}
        self.minAvg = minAvg
        self.avgRange = avgRange
        self.minStddev = minStddev
        self.stdRange = stdRange
        self.weightsMatrix = np.ones((nodes,nodes))*float('inf')
        self.N = np.zeros((nodes,nodes))
        for i in range(nodes):
            self.G.add_node(i, color="blue" if i in [self.start,self.goal] else "red", cost = 0 if i==self.start else float('inf'))
        for i in range(walks):
            self.random_walk(True, 10)
            
    def random_walk(self, init, minPath):
        if init:
            s = self.start
            ct = 1
            while s != self.goal:
                e = random.randint(self.start,self.goal)
                if ct < minPath and e == self.goal:
                    continue
                ct+=1
                self.G.add_edge(s,e)
                self.weightsMatrix[s,e] = 0
                self.distributions[(s,e)] = (random.random()*self.avgRange+self.minAvg, random.random()*self.stdRange+self.minStddev)
                s = e
        else:
            s = random.randint(self.start,self.goal)
            for i in range(path):
                e = random.randint(self.start,self.goal)
                self.G.add_edge(s,e)
                self.weightsMatrix[s,e] = 0
                s = e
    def greedy_dijkstra(self):
        visited = np.ones(self.nodes)
        dijkstraWeights = [float('inf')]*self.nodes
        dijkstraWeights[self.start] = 0
        parents = [None]*self.nodes
        for j in range(len(visited)):
            idx = np.argmin(np.multiply(dijkstraWeights, visited))
            for i in range(self.nodes):
                if self.weightsMatrix[idx,i] < np.inf:
                    dist = self.distributions[(int(idx),int(i))]
                    weight = max(0,random.gauss(dist[0], dist[1]))
                    print(idx, i, weight)
                    exit(0) #RESUME DEBUGGING HERE
                    if dijkstraWeights[i] > dijkstraWeights[idx]+weight:
                        dijkstraWeights[i] = dijkstraWeights[idx]+weight
                        parents[i] = idx
                    self.N[idx,i]+=1
                    self.weightsMatrix[idx,i] += (1/self.N[idx,i])*(weight-self.weightsMatrix[idx,i])
            visited[idx] = np.inf

        path = []
        cur = R.goal
        while cur != R.start:
            path = [cur] + path
            cur = parents[cur]
        return (dijkstraWeights[R.goal], path)

r = OnlineRoute(5,25,5,5,1,1)
print(r.G.edges)
print(r.greedy_dijkstra())
#nx.draw(r.G)#, nodelist = list(range(25)), node_color = ["blue"]+["red"]*23+["blue"])
#plt.show()
