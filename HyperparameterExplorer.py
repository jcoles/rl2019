from bandit import Bandit
import numpy as np
import matplotlib.pyplot as plt
NUM_ARMS = 10
GAME_ITERS = 2000
NUM_GAMES = 200
MIN_MEAN = -2
MAX_MEAN = 2
MIN_STDDEV = 0.5
MAX_STDDEV = 1.0

def explore(start, stop, step_size, func):
        hyperparameter = 0
        graphOptimality = []
        graphReward = []
        steps = []
        while hyperparameter <= stop:
            optimalChoices = 0
            avgReward = 0
            for i in range(NUM_GAMES):
                B = Bandit(NUM_ARMS, MIN_MEAN, MAX_MEAN, MIN_STDDEV, MAX_STDDEV)
                
                o, s = func(B, hyperparameter, GAME_ITERS)
                optimalChoices += o[-1]
                avgReward += s[-1]
                B.reset()
            graphOptimality.append(optimalChoices/NUM_GAMES*100)
            graphReward.append(avgReward/NUM_GAMES)
            steps.append(hyperparameter)
            hyperparameter += step_size
        return (np.array(graphOptimality), np.array(graphReward), np.array(steps))
        
opt_e, reward_e, xaxis_e = explore(0, 0.4, 0.01,Bandit.e_greedy)
opt_u, reward_u, xaxis_u = explore(0, 8, 0.4, Bandit.ucb)
opt_b, reward_b, xaxis_b = explore(0, 8, 0.4, Bandit.biased_greedy)

plt.plot(xaxis_e, opt_e, label="ε(ε-greedy)", color="red")
plt.plot(xaxis_u/20, opt_u, label= "c*0.05 (ucb)", color="blue")
plt.plot(xaxis_b/20, opt_b, label= "bias*0.05 (biased greedy)", color="green")
plt.xlabel("Hyperparameter value")
plt.ylabel("Percentage of optimal moves")
plt.title("Percentage of optimal moves by algorithm and hyperparameter at move %d"%GAME_ITERS)
plt.legend()
plt.show()

plt.plot(xaxis_e, reward_e, label="ε(ε-greedy)", color="red")
plt.plot(xaxis_u/20, reward_u, label= "c*0.05 (ucb)", color="blue")
plt.plot(xaxis_b/20, reward_b, label= "bias*0.05 (biased greedy)", color="green")
plt.xlabel("Hyperparameter value")
plt.ylabel("Average reward")
plt.title("Average reward by algorithm and hyperparameter at move %d"%GAME_ITERS)
plt.legend()
plt.show()
'''
fig = plt.figure()
xaxis0 = fig.add_subplot(111)
a,b,c,d = xaxis0.get_position().bounds
xaxis1 = xaxis0.twiny()
xaxis2 = xaxis0.twiny()
xaxis0.plot(xaxis_e,opt_e, color="red")
xaxis0.set_xlabel("ε(ε-greedy)", color="red")
xaxis1.plot(xaxis_u, opt_u, color="blue")
xaxis1.set_xlabel("c (UCB)",color="blue")
xaxis2.plot(xaxis_b, opt_b, color="green")
xaxis2.set_xlabel("bias (biased greedy)", color="green")
plt.show()
'''
#df = pd.DataFrame(data = {"e-greedy": opt, "xaxis0":xaxis})
#df["e-greedy"].plot(label="e-greedy", legend=True)
#df["xaxis0"].plot(label="epsilon")
#plt.show()
#plt.plot(xaxis, opt, label="e-greedy")
#plt.xlabel("Hyperparameter value")
#plt.ylabel("% optimal moves")
#plt.title("Rate of optimal moves as a function of hyperparameters
