#a neat little program for multi-armed bandit techniques
#idea from chapter 2 of Reinforcement Learning: An Introduction
#Jakob Coles
#June 3, 2019
#TODO: Hyperparameter explorer, gradient bandit, nonstationary bandit, online route finding application
import random
from sys import argv
import numpy as np

class Bandit:
    def __init__(self, K, minMean, maxMean, minStddev, maxStddev, seed_ = None):
        if seed_:
            random.seed(seed_)
        self.avgRewards = [random.random()*(maxMean-minMean)+minMean for i in range(K)]
        self.stddevRewards = [random.random()*(maxStddev-minStddev)+minStddev for i in range(K)]
        self.K = K
        self.Q = np.zeros(K)
        self.n = np.zeros(K)
        self.optimalChoice = np.argmax(np.array(self.avgRewards))
        
    def action(self, A):
        return random.gauss(self.avgRewards[A],self.stddevRewards[A])
    
    def greedy(self, iters):
        totalReward = 0.0
        optimalChoices = np.zeros(iters)
        stepReward = np.zeros(iters)
        for i in range(iters):
            maxActions = np.where(self.Q == np.max(self.Q))[0]
            A = random.choice(maxActions)
            optimalChoices[i] = (A == self.optimalChoice)
            reward = self.action(A)
            stepReward[i] = reward
            self.n[A]+=1
            self.Q[A] += (1/(self.n[A]))*(reward-self.Q[A])
            totalReward += reward
        return (optimalChoices, stepReward)
    
    def e_greedy(self, e, iters):
        totalReward = 0.0
        optimalChoices = np.zeros(iters)
        stepReward = np.zeros(iters)
        for i in range(iters):
            A = -1
            if random.random() > e:
                maxActions = np.where(self.Q == np.max(self.Q))[0]
                A = random.choice(maxActions)
            else:
                A = random.randint(0, self.K-1)
            optimalChoices[i] = (A == self.optimalChoice)
            reward = self.action(A)
            stepReward[i] = reward
            self.n[A]+=1
            self.Q[A] += (1/self.n[A])*(reward-self.Q[A])
            totalReward += reward
        return (optimalChoices, stepReward)

    def biased_greedy(self, bias, iters):
        self.Q += bias
        return self.greedy(iters)

    def ucb(self, c, iters):
        totalReward = 0.0
        optimalChoices = np.zeros(iters)
        stepReward = np.zeros(iters)
        for i in range(iters):
            Q2 = self.Q+np.where(self.n == 0, np.inf, c*np.sqrt(np.log(i+1)/self.n))
            maxActions = np.where(Q2 == np.max(Q2))[0]
            A = random.choice(maxActions)
            optimalChoices[i] = (A == self.optimalChoice)
            reward = self.action(A)
            stepReward[i] = reward
            self.n[A] += 1
            self.Q[A] += (1/self.n[A])*(reward-self.Q[A])
        return (optimalChoices, stepReward)
            
    def optimal(self, iters):
        totalReward = 0.0
        optimalChoices = np.zeros(iters)
        stepReward = np.zeros(iters)
        for i in range(iters):
            A = self.optimalChoice
            optimalChoices[i] = 1
            reward = self.action(A)
            stepReward[i] = reward
            self.n[A] += 1
            self.Q[A] += (1/self.n[A])*(reward-self.Q[A])
        return (optimalChoices, stepReward)
    
    def reset(self):
        self.Q = np.zeros(self.K)
        self.n = np.zeros(self.K)
