#A testbed for multi-armed bandit algorithms
#python testbed.py 100 10 5000 -2 2 0.5 1
from sys import argv
from bandit import Bandit
import matplotlib.pyplot as plt
import numpy as np

numGames = int(argv[1])
numArms = int(argv[2])
gameIters = int(argv[3])
minMean = float(argv[4])
maxMean = float(argv[5])
minStddev = float(argv[6])
maxStddev = float(argv[7])

optimalChoices = np.zeros(gameIters)
avgRewards = np.zeros(gameIters)
for i in range(numGames):
    B = Bandit(numArms, minMean, maxMean, minStddev, maxStddev)
    o, s = B.greedy(gameIters)
    optimalChoices += o
    avgRewards += s
    B.reset()


    
optimalChoices2 = np.zeros(gameIters)
avgRewards2 = np.zeros(gameIters)
for i in range(numGames):
    B = Bandit(numArms, minMean, maxMean, minStddev, maxStddev)
    o, s = B.e_greedy(0.1, gameIters)
    optimalChoices2 += o
    avgRewards2 += s
    B.reset()


optimalChoices3 = np.zeros(gameIters)
avgRewards3 = np.zeros(gameIters)
for i in range(numGames):
    B = Bandit(numArms, minMean, maxMean, minStddev, maxStddev)
    o, s = B.ucb(2, gameIters)
    optimalChoices3 += o
    avgRewards3 += s
    B.reset()

x = list(range(0,5000,50))
plt.plot(x,optimalChoices[::50]/numGames*100,label="Greedy")
plt.plot(x,optimalChoices2[::50]/numGames*100, label="0.1-greedy")
plt.plot(x,optimalChoices3[::50]/numGames*100, label="USB, c=2")
plt.xlabel("Iteration #")
plt.ylabel("% optimal moves")
plt.title("Rate of optimal moves as a function of time")
plt.legend()
plt.show()
plt.plot(x,avgRewards[::50]/numGames,label="Greedy")
plt.plot(x,avgRewards2[::50]/numGames,label="0.1-greedy")
plt.plot(x,avgRewards3[::50]/numGames,label="UCB, c=2")
plt.xlabel("Iteration #")
plt.ylabel("Avg reward")
plt.title("Average reward per move as a function of tie")
plt.legend()
plt.show()
